///////////////////////////////////////////////////////////////////////////////////////////////
/////////////custom event list admin for ready or not ready status event by shimon/////////////
///////////////////////////////////////////////////////////////////////////////////////////////

/**
 * 
 * 
 * @param Array $columns The existing columns
 * @return Array $filtered_columns The filtered columns
 */
function shim_event_modify_columns( $columns ) {

  // New columns to add to table
  $new_columns = array(
	'event_ready' => __( 'Event Ready', 'myplugin_textdomain' ),
	
  );
	
  
  // Combine existing columns with new columns
  $filtered_columns = array_merge( $columns, $new_columns );

  // Return our filtered array of columns
  return $filtered_columns;
}

// Let WordPress know to use our filter
add_filter('manage_tribe_events_posts_columns' , 'shim_event_modify_columns');

/**
 * @param String $column The name of the column being acted upon
 * @return void
 */
function shim_event_custom_column_content( $column ) {
  

  switch ( $column ) {

    case 'event_ready' :
      
      	$event_ready = ( get_field( 'event_ready' ) )? get_field( 'event_ready' ): '';
      	$event_ready_class = (  $event_ready === true )? 'ready' : 'not-ready';
      
      
      // Echo output and then include break statement
      echo "<span class='$event_ready_class'>";
      		echo ( $event_ready === true ? 'ready' : 'not ready' );
      echo "</span>";

      
      break;

      
  }
}

// Let WordPress know to use our action
add_action( 'manage_tribe_events_posts_custom_column', 'shim_event_custom_column_content' );

function my_manage_columns( $columns ) {
  unset($columns['wpseo-score']);
  unset($columns['wpseo-links']);
  unset($columns['wpseo-linked']);
  unset($columns['wpseo-score-readability']);
  unset($columns['recurring']);
  unset($columns['wpseo-title']);
  unset($columns['wpseo-metadesc']);
  unset($columns['comments']);
  unset($columns['recurring']);


  return $columns;
}

function my_column_init() {
  add_filter( 'manage_tribe_events_posts_columns' , 'my_manage_columns' );
}
add_action( 'admin_init' , 'my_column_init' );

add_action('admin_head', 'my_column_width');

function my_column_width() {
    echo '<style type="text/css">';
    echo '.column-tickets { text-align: center; width:10% !important; }';
    echo '.column-event_ready { text-align: center; width:10% !important; color: #000}';
    echo 'td.column-event_ready span.ready {  background-color:#00ff00; padding: 3px 10px; }';
    echo '</style>';
  
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//////////End custom event list admin for ready or not ready status event by shimon//////////////
/////////////////////////////////////////////////////////////////////////////////////////////////